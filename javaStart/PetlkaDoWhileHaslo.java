package javaStart;

import java.util.Scanner;

public class PetlkaDoWhileHaslo {
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        String password;
        do {
            System.out.println("podaj hasło ");
            password = odczyt.nextLine();

        } while (password.isEmpty() || password.length() < 5);
        System.out.println("Brawo hasło jest prawidłowe i spełnia wymogi ");
    }
}